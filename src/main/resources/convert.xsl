<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" />
	<xsl:template match="/">
		<variables>
			<kontaktstatus>
				<value>Unbekannt</value>
				<type>String</type>
			</kontaktstatus>
			<terminstatus>
				<value>Unbefugt</value>
				<type>String</type>
			</terminstatus>
			<auftragsdaten>
				<value><xsl:value-of select="./auftragsdaten" /></value>
				<type>Long</type>
			</auftragsdaten>
			<kondenkontaktid>
				<value>0</value>
				<type>Long</type>
			</kondenkontaktid>
			<vertreterid>
				<value>0</value>
				<type>Long</type>
			</vertreterid>
			<terminid>
				<value>0</value>
				<type>Long</type>
			</terminid>
		</variables>
	</xsl:template>
</xsl:stylesheet>